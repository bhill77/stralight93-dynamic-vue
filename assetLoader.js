// eslint-disable-next-line no-unused-vars
function loadDynamicAsset(url, id) {
  // eslint-disable-next-line no-async-promise-executor
  if (document.getElementById(id)) return;
  return new Promise(async (resolve, reject) => {
    var asset;
    if (url.trim().substr(-3).toLowerCase() === ".js") {
      asset = document.createElement("script");
      asset.addEventListener("load", resolve);
      asset.addEventListener("error", reject);
      asset.setAttribute("id", id);
      document.head.appendChild(asset);
      asset.setAttribute("src", url);
    } else {
      var link = document.createElement("link");
      link.rel = "stylesheet";
      link.href = url;
      link.setAttribute("id", id);
      link.addEventListener("load", resolve);
      link.addEventListener("error", reject);
      document.head.appendChild(link);
    }
  });
}
