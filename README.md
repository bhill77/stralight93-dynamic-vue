# Dynamic Runtime SFC Vue 3 Loader
Project ini telah berisi kumpulan komponen dan library yang dapat digunakan dalam project.
Berikut beberapa library yang telah terpasang:
- Windicss
- Pinia
- Vue 3
- Vue Router
- Fortawesome
- AG Grid Community
- Leaflet
- Sweetalert2
- Pako
- Localbase
- Vue Chartkick
- Vue Splide
- Vue Datepicker
- Vue qr
- dll

## Cara Kerja
- Copy **_config-example.json_** ke **_config-{port}.json_** atau **_config-{subdomain}.json_**
- Contoh jika anda punya domain _domain.com_ (bisa juga IP Address), maka:
    - jika menggunakan **_config-8000.json_**, maka aplikasi harus berjalan di **http://domain.com:8000**
    - jika menggunakan **_config-app.json_**, maka aplikasi harus berjalan di **http://app.domain.com**

## Keterangan config
```json
{
    "data": {
        "code": "Example",
        "name": "Example",
        "title": "Akan muncul di bagian sidebar atas",
        "url_template": "@templates", // lihat Folder Page
        "url_backend": "https:\/\/example.example.com",
        "url_menu": "endpoint setelah url_backend untuk listing menu sidebar",
        "url_socket": "url websocket jika diperlukan",
        "url_login": "path frontend ini untuk login default: /login",
        "url_landing": "path frontend ini untuk login default: /login",
        "url_user_check": "endpoint backend untuk cek auth user",
        "url_redirecting": "seperti landing, hanya saja lebih memaksa",
        "url_javascript": null,
        "version": 1,
        "description": null,
        "is_pwa_ready": null,
        "status": "ACTIVE",
        "public_path": [ "berisi URL public tanpa perlu terotentikasi" ],
        "theme": {
            "topBarColor":"bg-gray-100",
            "topBarText": "text-gray-800",
            "sideBarHeaderText":"!text-blue-800",
            "sideBarText":"text-gray-700",
            "sideBarBackground":"bg-gradient-to-b !text-white from-gray-100 to-white",
            "sideBarWidth" : "18.75rem"
        },
        "authenticate_every_page": false
    }
}
```

## Folder Halaman Vue Files
Dari contoh keterangan json di atas di bagian url_template tertulis **@templates**, bisa saja anda tulis endpoint dari url backend untuk response text file berisi kode vue js misal **/get-template**.

Artinya anda harus menaruh vue file di dalam project ini di dalam folder yang sama dengan config json, misal **@templates**, **@myproject**, atau apapun berawalan **@** sesuai yang dituliskan di key *url_template* di config json anda
1. Contoh Struktur Folder berpattern Module dengan susunan folder dan file:
    ```
    "/@templates" :{
        "/login" : [
            "login.vue",
            "loginFrm.vue"
        ]
    }
    ```
    struktur folder dan files di atas akan membentuk url di project ini:
    - /login
    - /login/:id

2. Contoh Struktur Folder berpattern Parent-Module dengan susunan folder dan file:
    ```
    "/@templates" :{
        "/parent" : {
            "/orderPage" : [
                "orderPage.vue",
                "orderPageFrm.vue"
            ]
        }
    }
    ```
    struktur folder dan files di atas akan membentuk url di project ini:
    - /parent-order_page
    - /parent-order_page/:id


Catatan: Pastikan semua Nama Folder dan File **menggunakan camelCase** meski path yang terbentuk snake case atau lainnya


## Credits
- [Bocah Ganteing](https://web.facebook.com/firmansyah.fajar)