/* eslint-disable no-undef */
importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
importScripts(
  "https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js"
);
importScripts("https://unpkg.com/localbase@0.7.5/dist/localbase.min.js");

firebase.initializeApp({
  apiKey: "AIzaSyAiLtKLcmr8Q1pzoT0nvYVpYDCXKVoh2Q0",
  authDomain: "isolva-erp-v1.firebaseapp.com",
  projectId: "isolva-erp-v1",
  storageBucket: "isolva-erp-v1.appspot.com",
  messagingSenderId: "637898935495",
  appId: "1:637898935495:web:01323b0640ce00cab442e2",
});

class CustomPushEvent extends Event {
  constructor(data) {
    super("push");

    Object.assign(this, data);
    this.custom = true;
  }
}
/*
 * Overrides push notification data, to avoid having 'notification' key and firebase blocking
 * the message handler from being called
 */
self.addEventListener("push", (e) => {
  // Skip if event is our own custom event
  if (e.custom) return;

  // Kep old event data to override
  const oldData = e.data;

  // Create a new event to dispatch, pull values from notification key and put it in data key,
  // and then remove notification key
  const newEvent = new CustomPushEvent({
    data: {
      ehheh: oldData.json(),
      json() {
        const newData = oldData.json();
        newData.data = {
          ...newData.data,
          ...newData.notification,
        };
        delete newData.notification;
        return newData;
      },
    },
    waitUntil: e.waitUntil.bind(e),
  });

  // Stop event propagation
  e.stopImmediatePropagation();

  // Dispatch the new wrapped event
  dispatchEvent(newEvent);
});

const messaging = firebase.messaging();
messaging.onBackgroundMessage(async (payload) => {
  // const hostName = self.location.hostname;
  const { title, body, icon, ...restPayload } = payload.data;
  let db = new Localbase("db");
  payload["title"] = payload.data.title;
  payload["_timestamp"] = payload.data._timestamp_unix;
	let isBackend = false

  if (title.startsWith("Server ")) {
		isBackend = true
    await db.collection("fcm").add(payload);
  } else {
    await db.collection("fcm_msg").add(payload);
  }

  self.clients
  .matchAll({ includeUncontrolled: true })
  .then(function (clients) {
    clients.forEach(function (client) {
      client.postMessage(isBackend?"fcm_command":"fcm_message");
    });
  });
  const notificationOptions = {
    body: isBackend ? 'Apakah anda masih aktif?':body,
    icon: icon || "/indonesia-flag.png", // path to your "fallback" firebase notification logo
    data: restPayload,
  };
  return self.registration.showNotification(title, notificationOptions);
});

self.addEventListener("notificationclick", (event) => {
  // console.log('[firebase-messaging-sw.js] notificationclick ', event); // debug info

  // click_action described at https://github.com/BrunoS3D/firebase-messaging-sw.js#click-action

  event.notification.close();
  return;

  if (event.notification.data && event.notification.data.click_action) {
    self.clients.openWindow(event.notification.data.click_action);
  } else {
    self.clients.openWindow(event.currentTarget.origin);
  }

  // close notification after click
  event.notification.close();
});
